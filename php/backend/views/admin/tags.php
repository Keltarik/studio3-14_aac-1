<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Теги';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-body">
        <div class="tag-index">

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    'id',
                    'name:ntext',
                    'slug:ntext',
                    [
                        'class' => 'yii\grid\DataColumn',
                        'value' => function ($model, $key, $index, $column)
                            {
                                return "<a class='btn btn-sm btn-primary' href='".\yii\helpers\Url::to(['/edit/tag', 'id' => $model->id])."'>Редактировать</a>"." ".
                                "<a class='btn btn-sm btn-danger' href='".\yii\helpers\Url::to(['/delete/tag', 'id' => $model->id])."'>Удалить</a>";
                            },
                        'format' => 'raw',
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>
