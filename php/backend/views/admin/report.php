<?php
/**
 * @var $reports \backend\models\type\Report[]
 */
?>

<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">Сообщения с сайта</h1>
	</div>
</div>

<div class="panel panel-default">
	<div class="panel-body">
		<table class="table table-striped table-bordered table-hover data-table">
			<thead>
			<tr>
				<th>От кого</th>
				<th>Что пишет</th>
			</tr>
			</thead>

			<tbody>

			<?php foreach ($reports as $report) { ?>
				<tr>
					<td>
						<div><h4><?= $report->getAttribute('user_name') ?></h4></div>
						<div><span><?= $report->getAttribute('user_phone') ?></span></div>
						<div><span><a
									href="mailto:<?= $report->getAttribute('user_email') ?>"><?= $report->getAttribute('user_email') ?></a></span>
						</div>
					</td>
					<td>
						<?php
						$project = $report->getProject();
						if ($project) {
							?>
							<div style="margin-bottom: 5px; font-size: 10px;">
								В проекте <a target="_blank"
														 href="<?= \yii\helpers\Url::to('@webUrl/' . $project->getAttribute('slug')) ?>"><?= $project->getAttribute('title') ?></a>
							</div>
							<?php
						}
						?>
						<div style="font-style: italic;">
							<?= $report->getAttribute('user_text') ?>
						</div>
					</td>
				</tr>
			<?php } ?>

			</tbody>
		</table>
	</div>
</div>
