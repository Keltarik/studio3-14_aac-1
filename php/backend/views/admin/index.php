<?php


/**
 * @var $this \yii\web\View
 */

$this->registerJsFile('@web/assets/angular.min.js', ['position' => $this::POS_END]);
$this->registerJsFile('@web/assets/ngDraggable.js', ['position' => $this::POS_END]);
$this->registerJsFile('@web/js/admin-angular.js', ['position' => $this::POS_END]);
?>
<div ng-app="studio">

	<input type="hidden" name="_csrf" value="<?= Yii::$app->request->getCsrfToken() ?>"/>


	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Главная</h1>
		</div>
	</div>
	<div class="row" ng-controller="Portfolio">


		<div class="col-xs-6">
			<h2>
				<a href="#" ng-click="hidePosition()">Портфолио</a>
			</h2>
		</div>
		<div class="col-xs-6">
			<h2 class="text-right"><a href="#" ng-click="showPosition()">Расположение</a></h2>
		</div>
		<div class="col-xs-12" ng-hide="position">
			<div class="row">
				<div style="min-height: 370px;" class="col-xs-{{project.size}}"
						 ng-repeat="project in json.portfolio  track by $index">

					<div style="background: rgba(0,0,0,.1); padding: 3px;">

						<div class="row">
							<div class="form-group col-xs-7">
								<label class="control-label" for="">Заголовок</label>
								<input type="text" ng-model="project.title" class="form-control">
							</div>
							<div class="form-group col-xs-5">
								<label class="control-label" for="">Мини текст</label>
								<input type="text" ng-model="project.description" class="form-control">
							</div>
						</div>
						<div class="row">

							<div class="form-group col-xs-8">
								<label class="control-label" for="">Проект</label>
								<select ng-model="project.slug" class="form-control">
									<option value="{{pr.slug}}" ng-selected="{{project.slug == pr.slug}}" ng-repeat="pr in json.projects">
										{{pr.title}}
									</option>
								</select>
							</div>
							<div class="form-group col-xs-4">
								<label class="control-label" for="">Размер</label>
								<select ng-model="project.size" class="form-control" convert-to-number>
									<option value="4">4</option>
									<option value="8">8</option>
								</select>
							</div>
						</div>
						<div class="row">

							<div class="col-xs-12">
								<label class="control-label" for=""> Изображение</label>
							</div>
							<div class="col-xs-4">
								<div class="form-group text-center">
									<img ng-src="{{project.image_url}}" class="img-responsive" alt="" style="height:50px;">

									<input type="hidden" ng-model="project.image">
								</div>
							</div>
							<div class="col-xs-8">
								<input type="file" class="upload-image" data-name="image" data-index="{{$index}}" name="Upload[image]">
							</div>
						</div>
                        <div class="row">
                            <div class="col-xs-12">
                                <label class="control-label" for="">Теги</label>
                                <select ng-model="project.tag_slug" class="form-control">
                                    <option value="{{tag.slug}}" ng-selected="{{tag.slug == project.tag_slug}}" ng-repeat="tag in json.tags">{{tag.name}}</option>
                                </select>
                            </div>
                        </div>
						<button ng-click="remove($index)" class="btn btn-sm btn-danger">Удалить</button>
					</div>

				</div>

				<div class="col-xs-{{new_portfolio.size}}" style="min-height: 300px;">

					<div style="background: rgba(0,0,0,.2); padding: 3px;">
						<div class="row">
							<div class="form-group col-xs-7">
								<label class="control-label" for="">Заголовок</label>
								<input type="text" ng-model="new_portfolio.title" class="form-control">
							</div>
							<div class="form-group col-xs-5">
								<label class="control-label" for="">Мини текст</label>
								<input type="text" ng-model="new_portfolio.description" class="form-control">
							</div>
						</div>

						<div class="row">
							<div class="form-group col-xs-8">
								<label class="control-label" for="">Проект</label>
								<select ng-model="new_portfolio.slug" class="form-control">
									<option value="{{pr.slug}}" ng-repeat="pr in json.projects">{{pr.title}}</option>
								</select>
							</div>
							<div class="form-group col-xs-4">
								<label class="control-label" for="">Размер</label>
								<select ng-model="new_portfolio.size" class="form-control">
									<option value="4">4</option>
									<option value="8">8</option>
								</select>
							</div>
						</div>

						<div class="row">
							<div class="col-xs-12">
								<label class="control-label" for=""> Изображение</label>
							</div>
							<div class="col-xs-4">
								<div class="form-group">
									<img ng-src="{{new_portfolio.image_url}}" class="img-responsive" alt="">

									<input type="hidden" ng-model="new_portfolio.image">
								</div>
							</div>
							<div class="col-xs-8">
								<input type="file" class="upload-image" data-name="image" name="Upload[image]">
							</div>
						</div>
                        <div class="row">
                            <div class="col-xs-12">
                                <label class="control-label" for="">Теги</label>
                                <select ng-model="new_portfolio.tag" class="form-control">
                                    <option value="{{tag}}" ng-repeat="tag in json.tags">{{tag}}</option>
                                </select>
                            </div>
                        </div>
						<button ng-click="addNew()" class="btn btn-sm btn-primary">Добавить</button>
					</div>
				</div>
			</div>
		</div>

		<div class="col-xs-12" ng-show="position">

			<div class="row">
				<div style="min-height: 200px;position: relative" class="col-xs-{{project.size}}"
						 ng-repeat="project in json.portfolio  track by $index"
						 ng-drag="true" ng-drag-data="project"
						 ng-drag-success="onDragComplete($data,$event)"

						 ng-drop="true"
						 ng-drop-success="onDropComplete($index, $data,$event)">

					<div
						style="background: rgba(0,0,0,.1) url('{{project.image_url}}') center no-repeat; background-size: cover; height: 170px; margin-top: 15px; padding: 3px;">
						<div class="row">
							<div class="col-xs-8">

								<h2 style="margin-left: 15px; color: white; text-shadow: 1px 1px 2px rgba(0,0,0,.4);">
									{{project.title}}</h2>
							</div>
						</div>
					</div>

				</div>
			</div>

		</div>
	</div>
</div>
