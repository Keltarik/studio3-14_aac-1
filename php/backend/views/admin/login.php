<?php
/**
 * @var $model \backend\models\Login
 */
?>
<div class="container">
	<div class="row">
		<div class="col-md-4 col-md-offset-4">
			<div class="login-panel panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Please Sign In</h3>
				</div>
				<div class="panel-body">
					<?php $form = \yii\widgets\ActiveForm::begin(['options' => ['class' => 'form-login form']]); ?>

					<?php if ($model->hasErrors()) { ?>
						<div class="alert alert-danger">Ошибка</div>
					<?php } ?>

					<fieldset>

						<?php /*
						$user = new \backend\models\User();
						$user->setAttribute('login', 'studio-admin');
						$user->setAttribute('password', Yii::$app->getSecurity()->generatePasswordHash('a$grLps1-4ivn3'));
						$user->setAttribute('token', sha1('a$grLps1-4ivn3'));

						$user->save();
						*/ ?>



						<?= $form->field($model, 'login')
							->textInput(
								['placeholder' => 'Логин',
									'class' => 'form-control']
							)->label('Логин') ?>


						<?= $form->field($model, 'password')
							->passwordInput(
								['placeholder' => 'Пароль',
									'class' => 'form-control']
							)->label('Пароль') ?>


						<!-- Change this to a button or input when using this as a form -->
						<button class="btn btn-lg btn-success btn-block">Login</button>
					</fieldset>

					<?php \yii\widgets\ActiveForm::end(); ?>
				</div>
			</div>
		</div>
	</div>
</div>
