<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\type\Slider */

$this->title = ($model->isNewRecord)?'Добавить элемент':'Редактировать элемент: ' . $model->text;
$this->params['breadcrumbs'][] = ['label' => 'Slider', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->text, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-body">
        <div class="tag-update">

            <?php $form = ActiveForm::begin([
                'successCssClass' => '',
                'options' => ['enctype' => 'multipart/form-data']
            ]); ?>

            <?= $form->field($model, 'text')->textInput() ?>

            <?= $form->field($model, 'link')->textInput() ?>
            <?php if (!$model->isNewRecord):?><img src="<?=Yii::getAlias('@uploads/slider/'.$model->picture)?>" width="300px"/><?php endif?>
            <?= $form->field($model, 'picture_file')->fileInput() ?>
            <?php if (!$model->isNewRecord):?><img src="<?=Yii::getAlias('@uploads/slider/'.$model->background)?>" width="300px"/><?php endif?>
            <?= $form->field($model, 'background_file')->fileInput() ?>

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>
