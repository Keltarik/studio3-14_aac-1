<?php
/**
 * Created by PhpStorm.
 * User: zachot
 * Date: 06.08.15
 * Time: 2:57
 */

namespace backend\models;


use yii\base\Model;

class Login extends Model {
	public $login;
	public $password;
	/**
	 * @var User
	 */
	private $_user;

	function rules() {
		return [
			[['login', 'password'], 'required'],
			[['login', 'password'], 'string'],
			[['password'], 'verifyPassword'],
		];
	}

	public function verifyPassword() {

		$this->_user = $this->getUser();
		if (!$this->_user)
			$this->addError('login', 'Incorrect login.');
		if ($this->_user && $this->_user->validatePassword($this->password))
			return true;
		else if (!$this->hasErrors())
			$this->addError('password', 'Incorrect password.');
		return false;

	}

	/**
	 * @return User|bool|null
	 */
	private function getUser() {
		return User::findOne(['login' => $this->login]);
	}

	function verify() {
		if (!\Yii::$app->request->isPost)
			return false;

		if ($this->load(\Yii::$app->request->post()) && $this->validate()) {
			if (\Yii::$app->user->login($this->_user, 3600 * 42))
				return true;
			else
				$this->addError('login', "Can't login to service");
		}
		return false;
	}
}