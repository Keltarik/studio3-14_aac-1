<?php
/**
 * Created by PhpStorm.
 * User: zachot
 * Date: 01.08.15
 * Time: 23:55
 */

namespace backend\controllers;


use backend\models\Login;
use backend\models\type\Project;
use backend\models\type\Teammate;
use backend\models\type\Partner;
use backend\models\type\Report;

use backend\models\User;
use common\models\type\Slider;
use common\models\type\Tag;
use yii\data\ActiveDataProvider;
use yii\web\Controller;


class AdminController extends Controller {

	public function beforeAction($action) {

		if (!parent::beforeAction($action))
			return false;
		if (YII_login) {
			switch ($action->id) {
				case "logout":
					break;
				case "login":
				case "registration":
					if (!\Yii::$app->user->getIsGuest()) {
						$this->redirect('@web/', 302);
						return false;
					}
					break;
				default:
					if (\Yii::$app->user->getIsGuest()) {
						$this->redirect('@web/login', 302);
						return false;
					}
			}
		}

		return true;
	}

	/**
	 * Выход пользователя
	 */
	public function actionLogout() {
		\Yii::$app->user->logout();
		return $this->redirect(array('login'), 302);
	}

	/**
	 * @return string
	 */
	public function actionLogin() {

		\Yii::$app->view->params['clear'] = 'clear';
		$model = new Login();

		if ($model->verify())
			return $this->redirect('@web', 302);

		return $this->render('login', ['model' => $model]);
	}

	public function actionIndex() {
		return $this->render('index');
	}

	public function actionProjects() {
		$projects = Project::find()->all();
		return $this->render('projects', ['projects' => $projects]);
	}

	public function actionTeam() {
		$team = Teammate::find()->all();
		return $this->render('team', ['team' => $team]);
	}

    public function actionPartners() {
        $partners = Partner::find()->all();
        return $this->render('partners', ['partners' => $partners]);
    }

	public function actionReport() {
		$reports = Report::find()->all();
		return $this->render('report', ['reports' => $reports]);
	}

    public function actionTags() {
        $dataProvider = new ActiveDataProvider([
            'query' => Tag::find(),
        ]);

        return $this->render('tags', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionSlider() {
        $dataProvider = new ActiveDataProvider([
            'query' => Slider::find(),
        ]);

        return $this->render('slider', [
            'dataProvider' => $dataProvider,
        ]);
    }
}