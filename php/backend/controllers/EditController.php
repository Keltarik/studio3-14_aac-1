<?php
/**
 * Created by PhpStorm.
 * User: zachot
 * Date: 02.08.15
 * Time: 1:25
 */

namespace backend\controllers;


use backend\models\edit\EditPartner;
use backend\models\edit\EditProject;
use backend\models\edit\EditTeammate;

use common\models\type\Slider;
use common\models\type\Tag;
use yii\web\Controller;
use Yii;
use yii\web\NotFoundHttpException;

class EditController extends Controller {

	function actionProject() {
		$id = \Yii::$app->request->get('id');
		$model = new EditProject();
		if ($id && is_numeric($id))
			$model->loadProject($id);

		if (\Yii::$app->request->isPost)
			if (!$model->go()) {
				if ($model->hasErrors('empty')) {
					$model->clearErrors('empty');
					return $this->redirect('@web/edit/project?id=' . $model->getId(), 302);
				}
			}

		return $this->render('project', ['model' => $model]);
	}

	function actionTeammate() {

		$id = \Yii::$app->request->get('id');

		$model = new EditTeammate();
		if ($id && is_numeric($id))
			$model->loadTeammate($id);

		if (\Yii::$app->request->isPost)
			if (!$model->go()) {
				if ($model->hasErrors('empty')) {
					$model->clearErrors('empty');
					return $this->redirect('@web/edit/teammate?id=' . $model->getId(), 302);
				}
			}

		return $this->render('teammate', ['model' => $model]);
	}

    function actionPartner() {

        $id = \Yii::$app->request->get('id');

        $model = new EditPartner();
        if ($id && is_numeric($id))
            $model->loadPartner($id);

        if (\Yii::$app->request->isPost)
            if (!$model->go()) {
                if ($model->hasErrors('empty')) {
                    $model->clearErrors('empty');
                    return $this->redirect('@web/edit/partner?id=' . $model->getId(), 302);
                }
            }

        return $this->render('partner', ['model' => $model]);
    }

    function actionTag($id) {
        if (($model = Tag::findOne($id)) !== null) {
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['/admin/tags']);
            } else {
                return $this->render('tag', [
                    'model' => $model,
                ]);
            }
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    function actionSlider($id) {
        if ((($model = Slider::findOne($id)) !== null) || $id == 0) {
            if ($id == 0)  {
                $model = new Slider();
                $model->scenario = 'create';
            }
            if ($model->load(Yii::$app->request->post()) && $model->go()) {
                return $this->redirect(['/admin/slider']);
            } else {
                return $this->render('slider', [
                    'model' => $model,
                ]);
            }
        } else {
            throw new NotFoundHttpException('Элемент не найден');
        }
    }
}