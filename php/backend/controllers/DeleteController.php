<?php

namespace backend\controllers;


use common\models\type\Slider;
use common\models\type\Tag;
use yii\web\Controller;
use Yii;
use yii\web\NotFoundHttpException;

class DeleteController extends Controller {

    function actionTag($id) {
        if (($model = Tag::findOne($id)) !== null) {
                $model->delete();
                return $this->redirect(['/admin/tags']);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    function actionSlider($id) {
        if (($model = Slider::findOne($id)) !== null) {
            @unlink(Yii::getAlias('@uploadsroot/slider/'.$model->picture));
            @unlink(Yii::getAlias('@uploadsroot/slider/'.$model->background));
            $model->delete();
            return $this->redirect(['/admin/slider']);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}