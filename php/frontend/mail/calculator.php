<?php
use yii\helpers\Html;
use yii\helpers\Url;


/**
 * @var $this \yii\web\View view component instance

 */

?>
<h1
    style="line-height: 32px;  font-size: 32px; border-left: 10px solid #10BDA9; margin-top: 10px; padding: 0 25px 0 15px">
    Рассчет проекта</h1>

<div style="padding: 0 25px;"><h4>Имя: <?= $project__user_name ?></h4></div>
<div style="padding: 0 25px;"><h5>Контакты:<?= $project__user_phone ?> <?= $project__user_email ?></h5></div>
<div style="padding: 0 25px;"><strong>Дизайн-проект: </strong> <?= $project__designe ?></div>
<div style="padding: 0 25px;"><strong>Площадь: </strong> <?= $project__area_width ?></div>
<div style="padding: 0 25px;"><strong>Новый дом: </strong> <?= !empty($project__new_home) ? 'Да' : 'Нет' ?></div>
<div style="padding: 0 25px;"><strong>Комнат: </strong> <?= $project__home ?></div>
<div style="padding: 0 25px;"><strong>Состав проживающих: </strong> <?= $project__male_female ?></div>
<div style="padding: 0 25px;"><strong>Нужно: </strong>
    <?php if (!empty($project__needed)): ?>
        <?php foreach ($project__needed as $need): ?>
            <div><?= $need ?></div>
        <?php endforeach; ?>
    <?php endif; ?>
</div>
<div style="padding: 0 25px;"><strong>Стилистически Вы больше тяготеете к: </strong>
    <?php if (!empty($project__user_style)): ?>
        <?php foreach ($project__user_style as $style): ?>
            <div><?= $style ?></div>
        <?php endforeach; ?>
    <?php endif; ?>
</div>