<?php
/**
 * Created by PhpStorm.
 * User: macos
 * Date: 08.10.17
 * Time: 17:00
 */

namespace frontend\widgets;

use common\models\type\Teammate;
use yii\base\Widget;
use yii\helpers\ArrayHelper;

class MainTeam extends Widget
{
    public function run()
    {
        $model = Teammate::find()
            ->orderBy(['position' => SORT_ASC])
            ->where(['main' => 1])
            ->all();

        return $this->render('main-team', [
            'model' => ArrayHelper::toArray($model)
        ]);
    }
}