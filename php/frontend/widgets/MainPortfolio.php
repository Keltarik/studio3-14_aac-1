<?php
/**
 * Created by PhpStorm.
 * User: macos
 * Date: 08.10.17
 * Time: 16:43
 */

namespace frontend\widgets;

use common\models\type\Tag;
use Yii;
use yii\base\Widget;
use yii\helpers\Json;

class MainPortfolio extends Widget
{
    private $_portfolio = null;

    public function init()
    {
        $path = Yii::getAlias('@webroot') . '/settings/portfolio.json';

        if (file_exists($path)) {
            if (($file = file_get_contents($path))) {
                $this->_portfolio = Json::decode($file);
            }
        }
    }

    public function run()
    {
        if (!$this->_portfolio) {
            return null;
        }

        $tagModel = Tag::find()->all();

        return $this->render('main-portfolio', [
            'tagModel' => $tagModel,
            'items' => $this->getItems()
        ]);
    }

    private function getItems()
    {
        $data = [];

        $count = 1;

        foreach ($this->_portfolio as $item) {
            $data[] = [
                'title' => isset($item['title']) ? $item['title'] : '',
                'image_url' => isset($item['image_url']) ? $item['image_url'] : '',
                'slug' => isset($item['slug']) ? $item['slug'] : '',
                'description' => isset($item['description']) ? $item['description'] : '',
                'tag_slug' => isset($item['tag_slug']) ? $item['tag_slug'] : ''
            ];

            if ($count == 10) {
                break;
            }else{
                $count ++;
            }
        }

        return $data;
    }
}