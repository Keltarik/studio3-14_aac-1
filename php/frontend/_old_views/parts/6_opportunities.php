<div id="opportunities" class="opportunities">
  <div class="container">
    <div class="row">
      <div class="col-xs-offset-1 col-xs-10"><h1> Наши преимущества</h1>
        <div class="opportunities-list">
	  <div class="opportunity">
            <div class="opportunity__shut">
              <div class="opportunity-circle">
                <img src="<?= \yii\helpers\Url::to("@web/img/old_template_img/6/form-0.png") ?>">
              </div>
            </div>
            <div class="opportunity__open">
              <div class="opportunity-content">
                <span style="background-image:url(<?= \yii\helpers\Url::to("@web/upload/photo/2016/02/26/a08b74ae7ec63755fcd7ccd6eeade3ad_13543.jpg") ?>)" class="author-img"></span>
                <span class="author-text">
                  <div class="author-text-content">«Мы&nbsp;работаем, учитывая ваши пожелания, и&nbsp;доступны в&nbsp;любое время, выходные и&nbsp;праздничные дни»</div>
                  <div class="author-text-nick">
                    <div class="author-text-name">Тимур Абдрахманов</div>
                    <div class="author-text-prof">руководитель студии</div>
                  </div>
                </span>
              </div>
            </div>
            <div class="opportunity-text">С&nbsp;нами удобно</div>
          </div>
          <div class="opportunity">
            <div class="opportunity__shut">
              <div class="opportunity-circle">
                <img src="<?= \yii\helpers\Url::to("@web/img/old_template_img/6/form-1.png") ?>">
              </div>
            </div>
            <div class="opportunity__open">
              <div class="opportunity-content">
                <span style="background-image:url(<?= \yii\helpers\Url::to("@web/upload/photo/2016/02/26/f7e839fcb78581bc2132ba521cbfa724_13539.jpg") ?>)" class="author-img"></span>
                <span class="author-text">
                  <div class="author-text-content">«Мы&nbsp;относимся к&nbsp;любому проекту с&nbsp;любовью частного мастера и&nbsp;с&nbsp;отвественностью серьезной организации»</div>
                  <div class="author-text-nick">
                    <div class="author-text-name">Альберт Янов</div>
                    <div class="author-text-prof">исполнительный директор</div>
                  </div>
                </span>
              </div>
            </div>
            <div class="opportunity-text">Любим свое дело</div>
          </div>
          <div class="opportunity">
            <div class="opportunity__shut">
              <div class="opportunity-circle">
                <img src="<?= \yii\helpers\Url::to("@web/img/old_template_img/6/form-2.png") ?>">
              </div>
            </div>
            <div class="opportunity__open">
              <div class="opportunity-content">
                <span style="background-image:url(<?= \yii\helpers\Url::to("@web/upload/photo/2016/02/26/34e36667bc6be3f10edfda342c1fc214_17047.jpg") ?>)" class="author-img"></span>
                <span class="author-text">
                  <div class="author-text-content">«Мы&nbsp;убеждены, что каждый интерьер должен иметь свою изюминку и&nbsp;историю, связанную с его хозяевами»</div>
                  <div class="author-text-nick">
                    <div class="author-text-name">Ирина Леготкина</div>
                    <div class="author-text-prof">старший дизайнер</div>
                  </div>
                </span>
              </div>
            </div>
            <div class="opportunity-text">Подходим<br/>нестандартно</div>
          </div>
          <div class="opportunity">
            <div class="opportunity__shut">
              <div class="opportunity-circle">
                <img src="<?= \yii\helpers\Url::to("@web/img/old_template_img/6/form-3.png") ?>">
              </div>
            </div>
            <div class="opportunity__open">
              <div class="opportunity-content">
                <span style="background-image:url(<?= \yii\helpers\Url::to("@web/upload/photo/2016/02/26/c229f4f3cbfaeaecc0fe940966d3be34_18570.jpg") ?>)" class="author-img"></span>
                <span class="author-text">
                  <div class="author-text-content">«Мы&nbsp;работаем более чем с сотней партнеров: производителями и&nbsp;поставщиками мебели, сантехники и&nbsp;отделочных материалов»</div>
                  <div class="author-text-nick">
                    <div class="author-text-name">Татьяна Кузнецова</div>
                    <div class="author-text-prof">руководитель группы комплектации</div>
                  </div>
                </span>
              </div>
            </div>
            <div class="opportunity-text">Знаем рынок</div>
          </div>
          <div class="opportunity">
            <div class="opportunity__shut">
              <div class="opportunity-circle">
                <img src="<?= \yii\helpers\Url::to("@web/img/old_template_img/6/form-4.png") ?>">
              </div>
            </div>
            <div class="opportunity__open">
              <div class="opportunity-content">
                <span style="background-image:url(<?= \yii\helpers\Url::to("@web/upload/photo/2016/02/26/7416d0464253fa821f86a1245f344822_14782.jpg") ?>)" class="author-img"></span>
                <span class="author-text">
                  <div class="author-text-content">«Знаем, как оптимизировать расходы на&nbsp;ремонт и&nbsp;сделать его более финансово прозрачным и предсказуемым»</div>
                  <div class="author-text-nick">
                    <div class="author-text-name">Борис Комаровский</div>
                    <div class="author-text-prof">руководитель проектной группы</div>
                  </div>
                </span>
              </div>
            </div>
            <div class="opportunity-text">Помогаем<br/>сэкономить</div>
          </div>
          <div class="opportunity">
            <div class="opportunity__shut">
              <div class="opportunity-circle">
                <img src="<?= \yii\helpers\Url::to("@web/img/old_template_img/6/form-5.png") ?>">
              </div>
            </div>
            <div class="opportunity__open">
              <div class="opportunity-content">
                <span style="background-image:url(<?= \yii\helpers\Url::to("@web/img/old_template_img/people/roman.jpg") ?>)" class="author-img"></span>
                <span class="author-text"><div class="author-text-content">«Мы&nbsp;умеем сделать всё так, как задумали, и&nbsp;при этом надежно и&nbsp;качественно»</div>
                  <div class="author-text-nick">
                    <div class="author-text-name">Роман Дорфман</div>
                    <div class="author-text-prof">«СПС-Техногрупп»,<br/> партнер «Студии 3.14»<br/> по ремонтным работам</div>
                  </div>
                </span>
              </div>
            </div>
            <div class="opportunity-text">Работаем<br/>на&nbsp;результат</div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
