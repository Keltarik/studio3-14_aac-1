<div class="wrapper-page">
	<div class="page">
		<div class="slider">
            <?php $i = 0?>
            <?php foreach (\common\models\type\Slider::find()->orderBy('position')->all() as $slider):?>
			<div data-slide="<?=$i++?>" class="slider-slide fade <?=$i==1?'in':''?>">
				<div style="background-image:url(<?= \yii\helpers\Url::to("@uploads/slider/".$slider->picture)?>)"
						 class="slider-slide-img">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12 text-right">
                                <div class="slider-slide-link">
                                    <a href="<?=$slider->link?>" target="_blank"><?=$slider->text?></a>
                                </div>
                            </div>
                        </div>
                    </div>
				</div>
			</div>
            <?php endforeach?>
			<div class="slider-title">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 text-right">
							<h1>Создаем<br/>пространства<br/>для жизни</h1><br/>
						</div>
					</div>
				</div>
			</div>
            <div class="slider-bottom-panel">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-3 col-sm-4">
                            <div class="slider-nav">
                                <?php for ($z = 0; $z < $i; $z++):?>
                                    <a data-target="<?=$z?>" class="slider-nav-link <?=$z==0?'active':''?>"></a>
                                <?php endfor?>
                            </div>
                        </div>
                        <div class="col-xs-9 col-sm-8 text-right">
                            <p>«Студия 3.14» — дизайн интерьера квартир, таунхаусов и домов в Москве, Подмосковье и других регионах</p>
                        </div>
                    </div>
                </div>
            </div>
		</div>

		<div class="page-bottom"></div>
	</div>
</div>
