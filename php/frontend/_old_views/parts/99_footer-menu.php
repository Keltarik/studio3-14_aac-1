<?php
/**
 */


use yii\helpers\Url;

?>

<div class="wrapper-footer">
    <?php if ($isHome) { ?><div id="map" style="height: 645px;"></div><?php } ?>
    <div id="contacts" class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12 col-sm-5">
                    <div class="footer-left">ООО «Студия&nbsp;3.14»<br/>ИНН/КПП 7736127922 /
                        77360010001<br/>ОГРН 1157746261628 <br/>Адрес: 119334, Москва,
                        Ленинский пр-т, 30А<br/>©&nbsp;<span class="text-bold"><nobr>2013–2017</nobr> «Студия&nbsp;3.14»</span>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-2">
                    <ul class="footer-list">
                        <!--li><a href="/#service" class="js-scroll-link"> Услуги</a></li-->
                        <li><a href="<?= Url::to("@web/") ?>#price"> Цены</a></li>
                        <li><a href="<?= Url::to("@web/") ?>#portfolio"> Портфолио</a></li>
                        <li><a href="<?= Url::to("@web/") ?>#team"> Наша команда</a></li>
                        <li><a href="<?= Url::to("@web/partners") ?>"> Партнеры</a></li>
                        <li><a href="<?= Url::to("@web/") ?>#contacts"> Контакты</a></li>
                    </ul>
                </div>
                <div class="col-xs-12 col-sm-5 text-right footer-contacts"><h2 class="footer-tel" onclick="yaCounter24474014.reachGoal('phone'); ga('send', 'event', 'Goal', 'phone'); roistat.event.send('phone');"><span class="roistat-phone">+7&nbsp;499&nbsp;398-17-33</span></h2>

                    <div><a href="mailto:info@studio3-14.ru"
                            class="footer-link footer-email" onclick="yaCounter24474014.reachGoal('email'); ga('send', 'event', 'Goal', 'email'); roistat.event.send('email');">info@studio3-14.ru</a></div>
                    <br>

                    <div><span>поставщикам для предложений о сотрудничестве:</span><br>
                        <a href="mailto:partner@studio3-14.ru"
                           class="footer-link footer-email" onclick="yaCounter24474014.reachGoal('email'); ga('send', 'event', 'Goal', 'email'); roistat.event.send('email');">partner@studio3-14.ru</a>
                        <br>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-8">*Oбращаем ваше внимание на&nbsp;то, что расчет
                    стоимости услуг на&nbsp;калькуляторе имеет предварительный характер и&nbsp;не
                    является коммерческим предложением или публичной офертой, определяемой
                    положениями Статьи 437 (2) Гражданского кодекса РФ.
                </div>
            </div>
        </div>
    </div>
    <div id="modal_form" class="white-popup-block mfp-hide">
        <h4>Пожалуйста, заполните телефон, чтобы мы&nbsp;могли связаться с&nbsp;вами.</h4><br>

        <div class="text-center">
            <button onclick="$.magnificPopup.close();"
                    class="btn btn-primary popup-modal-dismiss">Ok
            </button>
        </div>
    </div>
</div>
