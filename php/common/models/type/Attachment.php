<?php
/**
 * Created by PhpStorm.
 * User: zachot
 * Date: 04.08.15
 * Time: 2:14
 */

namespace common\models\type;


use yii\db\ActiveRecord;
use yii\helpers\Url;

class Attachment extends ActiveRecord {
	function getUrl() {
		return Url::to($this->getAttribute('src'));
	}

	/**
	 * @param string $src
	 * @return Attachment
	 */
	static function create($src){
		$_att = new Attachment();
		$_att->setAttribute('src', $src);
		$_att->save();
		return $_att;
	}
}