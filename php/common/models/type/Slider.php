<?php

namespace common\models\type;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "{{%slider}}".
 *
 * @property integer $id
 * @property string $picture
 * @property string $background
 * @property string $text
 * @property string $link
 */
class Slider extends \yii\db\ActiveRecord
{
    public $picture_file;
    public $background_file;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%slider}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text', 'link'], 'required'],
            [['picture_file', 'background_file'], 'required', 'on' => 'create'],
            [['text', 'link'], 'string'],
        	[['position'], 'number'],
            [['picture', 'background'], 'string', 'max' => 255],
            [['picture_file', 'background_file'], 'image'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'picture' => 'Картинка',
            'picture_file' => 'Картинка',
            'background' => 'Фон',
            'background_file' => 'Фон',
            'text' => 'Текст',
            'link' => 'Ссылка',
        	'position' => 'Позиция',
        ];
    }

    public function go()
    {
        $this->picture_file = UploadedFile::getInstance($this, 'picture_file');
        $this->background_file = UploadedFile::getInstance($this, 'background_file');
        if (!$this->validate()) return false;
        if ($this->picture_file) {
            $this->picture_file->saveAs(Yii::getAlias('@uploadsroot/slider/'.$this->picture_file->name), false);
            $this->picture = $this->picture_file->name;
        }
        if ($this->background_file) {
            $this->background_file->saveAs(Yii::getAlias('@uploadsroot/slider/'.$this->background_file->name), false);
            $this->background = $this->background_file->name;
        }
        if (!$this->save()) return false;
        return true;
    }
}
