<?php
/**
 * Created by PhpStorm.
 * User: zachot
 * Date: 05.08.15
 * Time: 12:34
 */

namespace common\models\type;


use yii\db\ActiveRecord;

class Review extends ActiveRecord {

	/**
	 * @return string
	 */
	function getPhotoUrl() {
		/**
		 * @var $photo Attachment
		 */
		$photo = Attachment::findOne(['id' => $this->getAttribute('author_photo')]);
		return $photo->getUrl();
	}

	/**
	 * @return null|Project
	 */
	function getProject() {
		return Project::find()
			->where('`review-ids` LIKE "%' . $this->getAttribute('id') . '%"')
			->one();
	}

	/**
	 * @return mixed
	 */
	function getVideoId() {
		parse_str(parse_url($this->getAttribute('video'), PHP_URL_QUERY), $my_array_of_vars);
		return $my_array_of_vars['v'];
	}
}