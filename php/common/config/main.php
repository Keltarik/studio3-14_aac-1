<?php
//$mail = require(__DIR__ . '/mail.php');

$config = [
	'id' => 'common',
	'basePath' => dirname(__DIR__),
	'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
	'components' => [
		/*'cache' => [
			'class' => 'yii\caching\FileCache',
		],*/
		'db' => require(__DIR__ . '/db.php'),
		//'mailer' => $mail,
	],
	'params' => require(__DIR__ . '/params.php'),
];

return $config;
