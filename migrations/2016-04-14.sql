CREATE TABLE `studia3-14`.`slider` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `picture` VARCHAR(255) NULL,
  `background` VARCHAR(255) NULL,
  `text` LONGTEXT NULL,
  `link` MEDIUMTEXT NULL,
  PRIMARY KEY (`id`));