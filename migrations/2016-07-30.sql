use studia314;
alter table slider add column `position` int(10) unsigned default 0;
update slider set position = id;
update slider set position = 0 where id = 2;
update slider set position = 1 where id = 3;
update slider set position = 2 where id = 4;
update slider set position = 3 where id = 5;
