(function () {
  'use strict';

  var jSelect = $('.calc-select');
  var jAnchor = $('.calc-result-anchor').first();
  var jResult = $('.calc-result-list-item');
  var jResultList = $('.service-calc-result');
  var jResultLink = $('.calc-result-link');
  var jResultType = $('.service-calc-result-type');

  var jDocument = $(document);
   

  var services = {
    0 : {
      type : "«Минимальный»",
      count : 4,
      pdf : "/web/pdf/minimal.pdf"
    },
    1 : {
      type : "«Базовый»",
      count : 5,
      pdf : "/web/pdf/basic.pdf"
    },
    2 : {
      type : "«Полный»",
      count : 7,
      pdf : "/web/pdf/full.pdf"
    },
    3 : {
      type : "«Под ключ»",
      count : 8,
      pdf : "/web/pdf/inclusive.pdf"
    }

  };

  jDocument.on('mousedown', '.calc-select', function () {
    setActive(this);
    //----
    var jThis = $(this);
    anchor(jThis.position().top + jThis.height() / 2 - 24 / 2);
    //----
    var tab = jThis.data('tab');

    if (tab === 'form')
      jResultList.addClass('service-calc-result__form');
    else {
      jResultList.removeClass('service-calc-result__form');
      setName(tab);
      setActiveResult(tab);
    }
  });

  function setName(index) {
    jResultType.text(services[index].type);
    jResultLink.attr('href', services[index].pdf);
  }

  function setActiveResult(index) {
    jResult.removeClass('active');
    for (var i = 0; i < services[index].count; i++)
      jResult[i].classList.add('active');
  }

  function setActive(node) {
    jSelect.removeClass('active');
    node.classList.add('active');
  }

  function anchor(top) {
    jAnchor[0].style.top = top + 'px';
  }
  
  // Блок услуги
  
  var currentIcon;
  
  jDocument.on('mousedown', '.service-icon', function () {
	  var text;
	  if (currentIcon) {
		  currentIcon.removeClass('service-hide');
		  text = currentIcon.find('.service-text')[0];
		  $(text).fadeOut(0);
	  }
	  var jThis = $(this);
	  if (!$(this).is(currentIcon)) {
		  jThis.addClass('service-hide');
		  text = jThis.find('.service-text')[0];
		  $(text).fadeIn("slow");
		  currentIcon = $(this);  
	  }
  });
}());