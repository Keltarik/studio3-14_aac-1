(function () {
  'use strict';

  $('.js-person').magnificPopup({
    type : 'ajax',
    alignTop : true,
    overflowY : 'scroll',

    removalDelay : 300,
    mainClass : 'mfp-fade',

    callbacks : {
      parseAjax : function (mfpResponse) {
        // mfpResponse.data is a "data" object from ajax "success" callback
        // for simple HTML file, it will be just String
        // You may modify it to change contents of the popup
        // For example, to show just #some-element:
        // mfpResponse.data = $(mfpResponse.data).find('#some-element');

        //mfpResponse.data = mfpResponse.data.html;
      }
    }
  });
})();