(function () {
    'use strict';
    var jPortfolio = $('.portfolio-gallery');
    var jPortfolioItems = jPortfolio.find('.portfolio-gallery-thumbnail');
    var jRow = $('<div />', {"class": 'row view'});
    var isPortfolioPage = window.location.href.indexOf('/portfolio') != -1;
    var row_count_enough_toggle = false;
    var visible_row_count = 4; // количество выводимых строк по-умолчанию
    var redirect_row_count = 6; // количество строк, при котором перенаправлять на портфолио
    $(document).on('mousedown', '.portfolio-gallery-more', function (event) {
        if (!row_count_enough_toggle) {
            event.stopPropagation();
            event.preventDefault();
        } else {return}
        var rows = jPortfolio.find('.row').not('.view');
        rows.first().slideDown(200).addClass('view');
        if (!isPortfolioPage && (jPortfolio.find('.row.view').length > redirect_row_count) || (rows.length <= 1)) {
            $(this).html('Все проекты').attr('href', '/portfolio');
            row_count_enough_toggle = true;
        }
    });
    
    function appendRow(jPortfolio, jRow, row_visible) {
    	if (!row_visible) {
    		jPortfolio.append($('<div />', {"class": 'row view'}).append(jRow.html()));
    	} else {
    		jPortfolio.append($('<div />', {"class": 'row'}).append(jRow.html()));
    	}
    }

    function drawItems(tag_slug) {
        jPortfolio.empty();
        jRow.empty();
        $('.portfolio-gallery-more').hide();
        var i = 0; // счётчик элементов с конкретным тегом
        var z = 0; // счётчик всех элементов
        var y = 0; // счётчик строк
        var row_visible = false;
        var length = jPortfolioItems.length;
        jPortfolioItems.each(function () {
            z++;
            if (y >= visible_row_count && !isPortfolioPage) {
                row_visible = true;
                $('.portfolio-gallery-more').html('Другие проекты').show();
            }
            if (($(this).data('tag-slug') == tag_slug) || (tag_slug == 'all')) {
                i++;
                jRow.append($('<div />', {
                    "class": 'col-sm-4 col-xs-12'}).append($('<div>').append($(this).clone()).html()));
                if (i == 3) {
                    appendRow(jPortfolio, jRow, row_visible);
                    y++;
                    i = 0;
                    jRow.empty();
                }
            }
            if ((i > 0) && (z == length)) {
            	appendRow(jPortfolio, jRow, row_visible);
                y++;
            }
        });
    }
    
    

    var jHash = window.location.hash.substring(window.location.hash.indexOf("#portfolio")+10);
    if (jHash) {
        $(document).scrollTop( $("#portfolio").offset().top );
        drawItems(jHash);
        $('#clear-tags').show();
        $('.tag-filter-link[data-id-slug="' + jHash + '"]').css('font-weight', '700');
    }

    $('.tag-filter-link').click(function () {
        $('.tag-filter-link').css('font-weight', 'normal');
        $(this).css('font-weight', '700');
        $('#clear-tags').show();
        drawItems($(this).data('id-slug'));
    });

    $('#clear-tags').click(function (event){
        event.stopPropagation();
        event.preventDefault();
        $('.tag-filter-link').css('font-weight', 'normal');
        $('#clear-tags').hide();
        drawItems('all');
    });

    $('#portfolio-link').click(function (){
        $('.tag-filter-link').css('font-weight', 'normal');
        $('#clear-tags').hide();
        drawItems('all');
    });
    $('#consulting-red-button').click(function (event) {
        if (isPortfolioPage){
            event.stopPropagation();
            event.preventDefault();
            $('html, body').animate({ scrollTop: document.getElementById('consulting').offsetTop });
}
});

}());