(function () {
    'use strict';
    var prices = [1800, 2200, 3500, 6000];
    var priceCategories = document.querySelectorAll(".price-category");
    var priceCategoriesArray = Array.prototype.slice.call(priceCategories);
    var priceDescriptions = document.querySelectorAll(".price-category-description");
    var priceDescriptionsArray = Array.prototype.slice.call(priceDescriptions);
    var area = 72;
    var selectedPrice = 2200;
    var priceInput = document.querySelector(".price-input");
    priceInput.addEventListener("input", areaChanger);

    function getSelectedPrice() {
        if (priceInput.value > 100 && priceInput.value <= 400) {
            var squareMeters = priceInput.value;
            var above100 = squareMeters - 100;
            var stepAbove100 = above100 / 2;
            var coeff = stepAbove100 * 0.2;
            var coeffPercent = (100 - coeff) / 100;
            return coeffPercent * selectedPrice;
        }
        else if (priceInput.value > 400) {
            return selectedPrice * 0.7;
        }
        return selectedPrice;
    }

    function calculation() {
        if (window.hasOwnProperty("yaCounter24474014")) {
            yaCounter24474014.reachGoal('calc');
        }
        ga('send', 'event', 'input', 'calc');
        var priceResultSpan = document.querySelector(".price-result-span");
        priceResultSpan.textContent = Math.round((area * getSelectedPrice())).toString();
        notice();
        discount();
        discount400();
    }

    function selectPrice(event) {
        var indexPrice = priceCategoriesArray.indexOf(event.target);
        for (var i = 0; i < priceCategoriesArray.length; i++) {
            var priceCategory = priceCategoriesArray[i];
            priceCategory.classList.remove("price-category-active");
        }
        event.target.className = "price-category price-category-active";
        for (var i = 0; i < priceDescriptionsArray.length; i++) {
            var priceDescription = priceDescriptionsArray[i];
            priceDescription.classList.remove("price-category-description-active");
        }
        priceDescriptionsArray[indexPrice].className = "price-category-description price-category-description-active";
        var currentPrice = prices[indexPrice];
        var spanCurrentPrice = document.querySelector(".price-span");
        spanCurrentPrice.textContent = currentPrice.toString();
        selectedPrice = currentPrice;
        calculation();
    }

    function areaChanger() {
        var priceInputValue = priceInput.value;
        area = priceInputValue;
        calculation();
    }

    for (var i = 0; i < priceCategories.length; i++) {
        var priceCategory = priceCategories[i];
        priceCategory.addEventListener("click", selectPrice);
    }

    function notice() {
        var priceResultSpan = document.querySelector(".price-result-span");
        var note = document.querySelector('.price-max60');
        var sqare = priceInput.value;
        if(sqare <= 60 && sqare !== '') {
            priceResultSpan.textContent = 60 * selectedPrice;
            note.classList.add('price-max60-visible');
        }
        else {
            note.classList.remove('price-max60-visible');
        }
    }

    function discount() {
        var discountNote = document.querySelector('.discount');
        var discountSpan = document.querySelector('.discount-span');
        if (priceInput.value > 100 && priceInput.value < 400) {
            discountNote.classList.add('discount-visible');
            discountSpan.textContent = area * selectedPrice - Math.round(area * getSelectedPrice());
        }
        else {
            discountNote.classList.remove('discount-visible');
        }
    }

    function discount400 () {
        var discount400Note = document.querySelector('.discount400');
        var discount400Span = document.querySelector('.discount400-span');
        if (priceInput.value >= 400) {
            discount400Note.classList.add('discount400-visible');
            discount400Span.textContent = Math.round(area * selectedPrice - area * selectedPrice * 0.7);
        }
        else {
            discount400Note.classList.remove('discount400-visible');
        }
    }

}());
