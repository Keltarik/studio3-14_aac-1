var Studio = angular.module('studio', ['ngDraggable'])
  .directive('convertToNumber', function () {
    return {
      require : 'ngModel',
      link : function (scope, element, attrs, ngModel) {
        ngModel.$parsers.push(function (val) {
          return parseInt(val, 10);
        });
        ngModel.$formatters.push(function (val) {
          return '' + val;
        });
      }
    };
  });

Studio.controller('Portfolio', ['$scope', '$http', function ($scope, $http) {

  function move(arr, old_index, new_index) {
    if (new_index >= arr.length) {
      var k = new_index - arr.length;
      while ((k--) + 1) {
        arr.push(undefined);
      }
    }
    arr.splice(new_index, 0, arr.splice(old_index, 1)[0]);
    return arr; // for testing purposes
  };

  var csrfToken = $('[name="_csrf"]').val();

  $scope.json = {};
  $scope.new_portfolio = {
    image_url : "",
    size : "4",
    tag : ""
  };
  $scope.position = false;

  $scope.hidePosition = function () {
    $scope.position = false;
  };
  $scope.showPosition = function () {
    $scope.position = true;
  };

  $scope.onDragComplete = function () {
    console.log('drag', arguments);
  };

  $scope.onDropComplete = function (index, obj, evt) {
    console.log('drop', arguments);

    var otherObj = $scope.json.portfolio[index];
    var otherIndex = $scope.json.portfolio.indexOf(obj);
    move($scope.json.portfolio, otherIndex, index);
    //$scope.json.portfolio[index] = obj;
    //$scope.json.portfolio[otherIndex] = otherObj;
  };

  $http.get('/admin/json/get-portfolio?_=' + Date.now())
    .then(function (res) {

      $scope.json.portfolio = res.data;
    });

  $http.get('/admin/json/projects?_=' + Date.now())
    .then(function (res) {
      $scope.json.projects = res.data;
      /*$scope.json.tags = [];
      $scope.json.projects.forEach(function(pr) {
          if (pr.tags != "") {
              var tgs = pr.tags.split(',');
              tgs.forEach(function(tg) {
                  if ($scope.json.tags.indexOf(tg.trim()) == -1) {
                      $scope.json.tags.push(tg.trim());
                  }
              })
          }
      });*/
    });

    $http.get('/admin/json/tags?_=' + Date.now())
        .then(function (res) {
            console.log(res.data);
            $scope.json.tags = res.data;
        });

  $scope.addNew = function () {


    $scope.json.portfolio.push($scope.new_portfolio);

    $scope.new_portfolio = {
      image_url : '',
      size : "4",
      tag : ""
    };

  };

  $scope.remove = function (key) {
    $scope.json.portfolio.splice(key, 1);
  };

  var saveAll = $scope.onSaveAll = function () {

    $.ajax('/admin/json/portfolio', {
      method : "post",
      dataType : "json",
      data : {'_csrf' : csrfToken, 'data' : $scope.json.portfolio}
    }).done(function (response) {
      console.log(response);
    });
  };

  $scope.$watch('json.portfolio', function (newValue, oldValue) {
    if (!newValue || !oldValue)
      return;
    saveAll();
  }, true);

  $(document).on('change', '.upload-image', function (event) {
    var file = event.target.files[0];

    var data = new FormData();
    data.append(event.target.name, file);
    data.append('name', event.target.getAttribute('data-name'));

    data.append('_csrf', csrfToken);

    var index = event.target.getAttribute('data-index');

    ajax('/admin/json/upload', data, function (response) {
      //console.log(response);
      if (response.success && !response.error) {
        if (index) {
          $scope.json.portfolio[index].image = response.file.id;
          $scope.json.portfolio[index].image_url = response.file.url;
        } else {
          $scope.new_portfolio.image = response.file.id;
          $scope.new_portfolio.image_url = response.file.url;
        }

        $scope.$apply();
      }
    })
  });
}]);
